<?php

namespace Drupal\cssvars_colorng;

class Ari {

  public static function color2rgbVector(AriColor $c) {
    return new Vector3($c->red, $c->green, $c->blue);
  }

  public static function color2hslVector(AriColor $c) {
    return new Vector3($c->hue, $c->saturation, $c->lightness);
  }

  public static function rgbVector2color(Vector3 $v) {
    return AriColor::newColor("rgb({$v->getA()}, {$v->getB()}, {$v->getC()})", 'rgb');
  }

  public static function hslVector2color(Vector3 $v) {
    return AriColor::newColor("hsl({$v->getA()}, {$v->getB()}, {$v->getC()})", 'hsl');
  }

}
