<?php

namespace Drupal\cssvars_colorng;

use Drupal\Component\Utility\Color;
use Drupal\Core\StringTranslation\TranslatableMarkup;

class ColorInfo {

  /**
   * @var static[]
   */
  private static $allColorInfo;

  /**
   * @return static[]
   */
  public static function all() {
    if (!isset(static::$allColorInfo)) {
      /** @var \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler */
      $themeHandler = \Drupal::service('theme_handler');

      // @fixme Care that we get the info untranslated.
      static::$allColorInfo = [];
      foreach ($themeHandler->listInfo() as $themeName => $themeInfo) {
        $colorInfo = new static($themeName);
        if ($colorInfo->getColorInfo()) {
          ColorInfo::$allColorInfo[$themeName] = $colorInfo;
        }
      }
    }
    return static::$allColorInfo;
  }

  /**
   * @param $themeName
   *
   * @return static|null
   */
  public static function get($themeName) {
    return static::all()[$themeName] ?? NULL;
  }

  /**
   * @var string
   */
  private $themeName;

  /**
   * @var array
   */
  private $colorInfo;

  /**
   * ColorInfo constructor.
   *
   * @param $colorInfo
   */
  public function __construct($themeName) {
    $this->themeName = $themeName;
    $this->colorInfo = $this->fetchColorInfo($themeName);
  }

  /**
   * @param string $themeName
   *
   * @return array
   */
  private function fetchColorInfo($themeName) {
    // Copied from color_get_info().
    $path = drupal_get_path('theme', $themeName);
    $file = \Drupal::root() . '/' . $path . '/color/color.inc';
    if ($path && file_exists($file)) {
      $info = [];
      // This sets $info;
      include $file;
      // Add in default values.
      $info += [
        // CSS files (excluding @import) to rewrite with new color scheme.
        'css' => [],
        // Files to copy.
        'copy' => [],
        // Gradient definitions.
        'gradients' => [],
        // Color areas to fill (x, y, width, height).
        'fill' => [],
        // Coordinates of all the theme slices (x, y, width, height) with their
        // filename as used in the stylesheet.
        'slices' => [],
        // Reference color used for blending.
        'blend_target' => '#ffffff',
      ];
      // @todo Bail out for missing keys:
      // - fields
      // - ['schemes']['default']['colors']
      return $info;
    }
  }

  /**
   * @return array
   */
  public function getColorInfo() {
    return $this->colorInfo;
  }

  /**
   * @return string
   */
  public function getThemeName() {
    return $this->themeName;
  }

  public function getParameterLabels() {
    return $this->colorInfo['fields'];
  }

  public function getUntranslatedParameterLabels() {
    return array_map(function ($label) {
      if ($label instanceof TranslatableMarkup) {
        return $label->getUntranslatedString();
      }
      else {
        return $label;
      }
    }, $this->colorInfo['fields']);
  }

  public function getCssPaths() {
    return $this->colorInfo['css'];
  }

  public function getFilePathsToCopy() {
    return $this->colorInfo['copy'];
  }

  public function getReferenceColor() {
    return $this->colorInfo['blend_target'];
  }

  public function getDefaultColors($normalize = TRUE) {
    $defaultColors = $this->colorInfo['schemes']['default']['colors'];
    if ($normalize) {
      foreach ($defaultColors as &$defaultColor) {
        $defaultColor = static::normalizeColor($defaultColor);
      }
    }
    return $defaultColors;
  }

  /**
   * @param string $color
   *
   * @return string
   */
  public static function normalizeColor($color) {
    $color = mb_strtolower($color);
    $color = Color::normalizeHexLength($color);
    return $color;
  }

}
