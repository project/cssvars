<?php

namespace Drupal\cssvars_colorng;

use Drupal\Core\Asset\CssOptimizer;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\cssvars\CssVars;
use function drupal_get_path;
use const FALSE;
use function file_exists;

class CssVarSubstitutor {

  /**
   * @param $themeName
   * @return static
   */
  public static function build($themeName) {
    return (new static($themeName));
  }

  /**
   * @var string
   */
  private $themeName;

  /**
   * @var \Drupal\cssvars_colorng\ColorInfo
   */
  private $colorInfo;

  /**
   * @var string[]
   */
  private $cssPathMap;

  /**
   * @var string[]
   */
  private $additionalCssPaths;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * CssVarSubstitutor constructor.
   *
   * @param $themeName
   */
  public function __construct($themeName) {
    $this->themeName = $themeName;
    $this->colorInfo = ColorInfo::get($themeName);
    if (!$this->colorInfo) {
      throw new \UnexpectedValueException("No ColorInfo found for theme $themeName");
    }

    $this->fileSystem = \Drupal::service('file_system');
  }

  /**
   * @return string
   */
  public function getThemeName() {
    return $this->themeName;
  }

  /**
   * @return string[]
   */
  public function getCssPathMap() {
    if (!isset($this->cssPathMap)) {
      $this->doBuild();
    }
    return $this->cssPathMap;
  }

  /**
   * @return string[]
   */
  public function getAdditionalCssPaths() {
    if (!isset($this->additionalCssPaths)) {
      $this->doBuild();
    }
    return $this->additionalCssPaths;
  }

  private function doBuild() {
    // Adapted from @see color_scheme_form_submit().
    $cssRelPaths = $this->colorInfo->getCssPaths();
    $themeDir = drupal_get_path('theme', $this->themeName);
    $targetDir = "public://cssvars_colorng/$this->themeName";
    $filesToCopy = $this->colorInfo->getFilePathsToCopy();

    // Copy over neutral images.
    foreach ($filesToCopy as $file) {
      $base = $this->fileSystem->basename($file);
      $source = "$themeDir/$file";
      try {
        $filepath = $this->fileSystem->copy($source, "$targetDir/$base");
      } catch (FileException $e) {
        $filepath = FALSE;
      }
      $paths['map'][$file] = $base;
      $paths['files'][] = $filepath;
    }

    $pathMap = [];
    foreach ($cssRelPaths as $cssRelPath) {
      $sourcePath = "$themeDir/$cssRelPath";
      $targetPath = "$targetDir/$cssRelPath";
      if (file_exists($sourcePath)) {
        $cssOptimizer = new CssOptimizer();
        // Aggregate @imports recursively for each configured top level CSS file
        // without optimization. Aggregation and optimization will be
        // handled by drupal_build_css_cache() only.
        $style = $cssOptimizer->loadFile($sourcePath, FALSE);

        list($style, $mixVars) = $this->rewriteStylesheet($style);
        $targetFileDir = $this->fileSystem->dirname($targetPath);
        $this->fileSystem->prepareDirectory($targetFileDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
        $this->fileSystem->saveData($style, $targetPath, FileSystemInterface::EXISTS_REPLACE);
        $pathMap[$cssRelPath] = $targetPath;
      }
    }

    // Write the css variables file and return its path by reference.
    $additionalPath = "$targetDir/cssvars.css";
    $vars = CssVars::varsToString($this->themeName, $this->defaultVars() + $mixVars);
    // Use :root selector so that cssvars can trump via html:root.
    $css = ":root {\n" . $vars . "\n}";
    $additionalPathDir = $this->fileSystem->dirname($additionalPath);
    $this->fileSystem->prepareDirectory($additionalPathDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $this->fileSystem->saveData($css, $additionalPath, FileSystemInterface::EXISTS_REPLACE);

    $this->cssPathMap = $pathMap;
    $this->additionalCssPaths = [$additionalPath];
  }

  private function rewriteStylesheet($style) {
    $defaultColors = $this->colorInfo->getDefaultColors();

    // Adapted from @see _color_rewrite_stylesheet
    // Split off the "Don't touch" section of the stylesheet.
    $split = "Color Module: Don't touch";
    if (strpos($style, $split) !== FALSE) {
      list($style, $fixed) = explode($split, $style);
    }

    // Find all colors in the stylesheet and the chunks in between.
    $style = preg_split('/(#[0-9a-f]{6}|#[0-9a-f]{3})/i', $style, -1, PREG_SPLIT_DELIM_CAPTURE);
    $is_color = FALSE;
    $output = '';
    $base = 'base';
    $mixVars = [];

    // Iterate over all the parts.
    foreach ($style as $chunk) {
      if (!isset($defaultColors[$base])) {
        $base = key($defaultColors);
      }

      if ($is_color) {
        $chunk = ColorInfo::normalizeColor($chunk);
        // Check if this is one of the colors in the default palette.
        if ($key = array_search($chunk, $defaultColors)) {
          $chunk = 'var(' . CssVars::varName($this->themeName, $key) .  ')';
        }
        // Not a pre-set color.
        else {
          $varKey = $base . '-' . substr($chunk, 1);
          $mixVars[$varKey] = $this->makeColorMixFormula($base, $chunk);
          $chunk = 'var(' . CssVars::varName($this->themeName, $varKey) .  ')';
        }
      }
      else {
        // Determine the most suitable base color for the next color.

        // 'a' declarations. Use link.
        if (preg_match('@[^a-z0-9_-](a)[^a-z0-9_-][^/{]*{[^{]+$@i', $chunk)) {
          $base = 'link';
        }
        // 'color:' styles. Use text.
        elseif (preg_match('/(?<!-)color[^{:]*:[^{#]*$/i', $chunk)) {
          $base = 'text';
        }
        // Reset back to base.
        else {
          $base = 'base';
        }
      }
      $output .= $chunk;
      $is_color = !$is_color;
    }

    // Append fixed colors segment.
    if (isset($fixed)) {
      $output .= $fixed;
    }

    return [$output, $mixVars];
  }

  private function defaultVars() {
    $vars = [];
    foreach ($this->colorInfo->getDefaultColors() as $varName => $varValue) {
      // Ex: base=#abcdef
      $vars[$varName] = $varValue;
      // For the mix forulas, we need HSL vars.
      // (Color functions come no sooner than CSS4.)
      $color = AriColor::newColor($varValue);
      $vars["$varName-hue"] = $color->hue;
      $vars["$varName-saturation"] = $color->saturation;
      $vars["$varName-lightness"] = $color->lightness;
    }
    return $vars;
  }

  /**
   * Make color mix formula.
   *
   * This makes a hsl formula for a mixed color.
   * In vectors: Find a scalar a and a vector B such that...
   *   original = reference + a*(originalBase-reference) + B
   * and output
   *   current = reference + a*(currentBase-reference) + B
   *
   * As reference is a constant and currentBase is a variable triplet,
   * we have
   *   current = foo + a*(currentBase-reference)
   * with
   *   foo = reference + B
   *
   * @see _color_shift()
   *
   * @param $baseParameterName
   *   The base parameter name.
   * @param $hexOriginalColor
   *   The original color.
   *
   * @return string
   *   The formula.
   */
  private function makeColorMixFormula($baseParameterName, $hexOriginalColor) {
    $hexReferenceColor = $this->colorInfo->getReferenceColor();
    $defaultColors = $this->colorInfo->getDefaultColors();
    $hexOriginalBaseColor = $defaultColors[$baseParameterName];

    $originalColor = AriColor::newColor($hexOriginalColor, 'hex');
    $originalBaseColor = AriColor::newColor($hexOriginalBaseColor, 'hex');
    $referenceColor = AriColor::newColor($hexReferenceColor, 'hex');

    // In RGB make (base - ref), and length.
    $baseRgb = Ari::color2rgbVector($originalBaseColor);
    $refRgb = Ari::color2rgbVector($referenceColor);
    $diffBaseFromRef = $baseRgb->vectorDiff($refRgb);
    $lenBaseFromRef = sqrt($diffBaseFromRef->vectorMult($diffBaseFromRef));

    // Project (original - base) onto that, divided by both lengths, to get a.
    // Determine projected (original on base-ref)
    $originalRgb = Ari::color2rgbVector($originalColor);
    $diffOriginalFromRef = $originalRgb->vectorDiff($refRgb);
    $lenOriginalFromRef = sqrt($diffOriginalFromRef->vectorMult($diffOriginalFromRef));
    $scalarA = $diffOriginalFromRef->vectorMult($diffBaseFromRef)
      / $lenBaseFromRef / $lenOriginalFromRef;
    $projectedRgb = $refRgb->vectorAdd($diffBaseFromRef->scalarMult($scalarA));

    // Determine B, in HSL space.
    $originalHsl = Ari::color2hslVector($originalColor);
    $projectedHsl = Ari::color2hslVector(Ari::rgbVector2color($projectedRgb));
    $vectorBasHsl = $originalHsl->vectorDiff($projectedHsl);

    // determine Foo
    $refHsl = Ari::color2hslVector($referenceColor);
    $vectorFooHsl = $vectorBasHsl->vectorAdd($refHsl);
    $colorFoo = Ari::hslVector2color($vectorFooHsl);

    // Make the formula.
    // Ex Hue: 70 + 0.123 * (var(--bartik--base-hue) - 23)
    // Ex S/L: 70% + 0.123 * (var(--bartik--base-saturation) - 23%)
    $template = '{foo}+{a}*(var({prefix}-{part})-{ref})';

    $common = [
      '{a}' => round($scalarA, 3),
      '{prefix}' => CssVars::varName($this->themeName, $baseParameterName),
    ];
    $hue = strtr($template, $common + [
      '{foo}' => round($colorFoo->hue),
      '{part}' => 'hue',
      '{ref}' => round($referenceColor->hue),
    ]);
    $saturation = strtr($template, $common + [
      '{foo}' => round($colorFoo->saturation) . '%',
      '{part}' => 'saturation',
      '{ref}' => round($referenceColor->saturation) . '%',
    ]);
    $lightness = strtr($template, $common + [
      '{foo}' => round($colorFoo->lightness) . '%',
      '{part}' => 'lightness',
      '{ref}' => round($referenceColor->lightness) . '%',
    ]);
    $formula = "hsl(calc($hue), calc($saturation), calc($lightness))";
    return $formula;
  }

}
