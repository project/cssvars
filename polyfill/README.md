# CSSVars Polyfill

This adds the [jhildenbiddle/css-vars-ponyfill](https://github.com/jhildenbiddle/css-vars-ponyfill)
to use css variables in IE browsers.

To use a local version of the js run (in Drupal root):

`mkdir -p libraries/css-vars-ponyfill && wget https://cdn.jsdelivr.net/npm/css-vars-ponyfill@2 -O libraries/css-vars-ponyfill/css-vars-ponyfill.js`
